﻿using UnityEngine;
using System.Collections;

public class CameraSwitcher : MonoBehaviour {
	public Camera myCam;
	public NetworkView netView;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (netView != null && netView.isMine)
		if (myCam.enabled == false)
			myCam.enabled = true;

	}
}
