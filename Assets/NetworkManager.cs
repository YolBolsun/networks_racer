﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class NetworkManager : MonoBehaviour {

	private string typeName = "NetworkRacer";//TODO make typename unique
	private string gameName = "Player 1";//TODO make this current person's username
	//MasterServer.ipAddress = “127.0.0.1″; //TODO change this to my server's ip address
	public GameObject playerPrefab;
	private GameObject player;
	public NetworkView nView;
	private Vector3 myVec = new Vector3(0,0,0);

	private void SpawnPlayer(){
		NetworkPlayer[] players = Network.connections;
		nView.RPC ("WhoAmI", RPCMode.Others, null);
		//myVec = new Vector3(0f+players.Length,.5f,0f);
		player = (GameObject)Network.Instantiate (playerPrefab, myVec,Quaternion.identity, 0 );


	}
	public GameObject GetMe(){
		return player;
	}

	private void StartServer()
	{
		Network.InitializeServer(4, 2000, !Network.HavePublicAddress());
		//gameName = gameName + Time.fixedTime.ToString();
		MasterServer.RegisterHost(typeName, gameName);
	}

	[RPC]
	void StartGame()
	{
		if (Network.isServer) {
			nView.RPC ("StartGame", RPCMode.Others, null);
		}
		if (!Network.isServer) {
			player.transform.position = myVec;
			player.transform.rotation = Quaternion.identity;
		}
	}

	void OnServerInitialized()
	{
		//SpawnPlayer ();
		Debug.Log("Server Initializied");
	}

	void OnGUI()
	{
		if (!Network.isClient && !Network.isServer) {
			if (GUI.Button (new Rect (100, 100, 250, 100), "Start Server"))
				StartServer ();
			
			if (GUI.Button (new Rect (100, 250, 250, 100), "Refresh Hosts")) {
				RefreshHostList ();
				//MasterServer.RegisterHost (typeName + DateTime.Now.Second.ToString (), gameName + DateTime.Now.Second.ToString ());
			}
			
			if (hostList != null) {
				for (int i = 0; i < hostList.Length; i++) {
					if (GUI.Button (new Rect (400, 100 + (110 * i), 300, 100), hostList [i].gameName))
						JoinServer (hostList [i]);
				}
			}
		} else if (Network.isServer) {
		
			if (GUI.Button (new Rect (250, 100, 250, 100), "Start Game"))
				StartGame();
		}
	}

	private HostData[] hostList;
	
	private void RefreshHostList()
	{
		MasterServer.RequestHostList(typeName);
	}

	void OnPlayerConnected(){
		if (2 == Network.connections.Length && Network.isServer) {
			StartGame ();
		}
	}
	
	void OnMasterServerEvent(MasterServerEvent msEvent)
	{
		if (msEvent == MasterServerEvent.HostListReceived)
			hostList = MasterServer.PollHostList();
	}

	private void JoinServer(HostData hostData)
	{
		Network.Connect(hostData);
	}
	
	void OnConnectedToServer()
	{
		SpawnPlayer ();
		Debug.Log("Server Joined");
	}

	[RPC]
	void YouLose(){
		Winning Win = GameObject.FindGameObjectsWithTag ("WinningTag")[0].GetComponent<Winning>();
		Debug.Log ("You Lose");
		Win.YouLose();
	}
	[RPC]
	void SetStartNumber(float f){
		if(myVec.magnitude == 0)
			myVec = new Vector3(0f+f,.5f,0f);
	}
	[RPC]
	void WhoAmI(){
		nView.RPC ("SetStartNumber", RPCMode.Others, (float)Network.connections.Length);
	}


	void OnApplicationQuit(){
		Network.Destroy (player);
	}

	// Use this for initialization
	void Start () {
	//	typeName = "NetworksRacer" + (System.DateTime.Now.Millisecond * GetInstanceID() %5000 + 1);
	//	gameName = "player " + (System.DateTime.Now.Millisecond * GetInstanceID() %10000);
	}
	
	// Update is called once per frame
	void Update () {

	}
}
