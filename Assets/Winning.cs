using UnityEngine;
using System.Collections;

public class Winning : MonoBehaviour {
	bool win = false;
	bool Lost = false;
	//for some reason all of these values are being shared at run time among the prefabs
	public NetworkView nView;

	public void FinishLine(){
		Debug.Log ("You Win");
		win = true;
		nView.RPC ("YouLose", RPCMode.Others, null);
	}

	public void YouLose(){
		Debug.Log ("You Lose");
		Lost = true;
	}

	void OnGUI(){
		GUIStyle st = new GUIStyle ();
		st.fontSize = 40;
		if (win) {
			GUI.TextArea (new Rect (100, 100, 50, 50), "You Win!", st);
		} else if (Lost) {
			GUI.TextArea (new Rect (100, 100, 50, 50), "You Lose!", st);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
