﻿using UnityEngine;
using System.Collections;

public class playerMovement : MonoBehaviour {
	static Vector3 right = new Vector3(1,0,0);
	static float speed = 15;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<NetworkView>().isMine) {
			Vector3 moveDirection = new Vector3(0,0,0);
			CharacterController controller = GetComponent<CharacterController>();
			if (Input.GetKey (KeyCode.LeftArrow))
				moveDirection = -1*right;
			if (Input.GetKey (KeyCode.RightArrow))
				moveDirection = right;
			if (Input.GetKey (KeyCode.UpArrow))
				moveDirection = transform.forward;
			if (Input.GetKey (KeyCode.DownArrow))
				moveDirection = -1 * transform.forward;
			moveDirection *= speed;
			controller.Move (moveDirection * Time.deltaTime);
		}
	}
}
