Jonathan Clinton and Nathan Walker 
4/8/2015 WIP submission

testNetwork2.exe is the current build in unity

We were having a little bit of trouble getting all of the networks stuff working with the third person character controller so we went ahead and went back to blocks.

Right now it spawns a new unique character for each player (though at the moment you still just sign in as player 1)

The blocks can then be moved around and updated on real time to all users

First start it up and start the server which will also create a block that you can control with the arrow keys then with the others hit refresh hosts and then player 1. 

We plan to have this all working with actual characters so the camera will also follow whoever you are by the submission on the 13th. From there it will just be cleaning things up and fleshing things out so that we can have all the game logic winning/losing and one complete level done by the 20th.

~~~~~~~~ 4/13/2015 submission ~~~~~~~~
First person camera is implemented. we can keep it this way, or adjust it to third person.
Per-player camera achieved by attaching cameras to all characters and deciding which one belongs to "me."
Still need:
	- prettier models
	- prettier level
	- win/loss logic
	- maybe a more intelligent (smooth, maybe mouse look) follow camera?

~~~~~~~~ 4/16/2015 update ~~~~~~~~
Finally got the 3rd person player controllers synchronizing over the network though for some reason, the animations are only running for the character that you control. Also added in a terrain so that we can start working on the level.
Still need:
	- change the terrain to make a good level
	- win/loss logic
	- game start logic
	- hopefully fix the animation on the other characters
~~~~~~~~ 4/19/2015 update ~~~~~~~~
Made the Terrain into a somewhat workable test level
Got win/loss logic working
Have Game Start currently tied to a button for server program to press
Upon disconnect, players are now removed from scene
Still need:
	- fix the animation on characters
	- switch to automatic game start
~~~~~~~~ 4/20/2015 Final Submission ~~~~~~~~
You can hold shift to run faster.
At the moment, the finish line is set at the end of the world. Follow the grey.
Upon a second client joining, the game automatically starts, or the server has a start game button that can be used.
testNetwork.exe is the newest compiled version